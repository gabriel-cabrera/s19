// Conditional Statements - allows us to control the flow of our program.


// [SECTION] if, else if, else Statements.


/*
Syntax: 

	if(condition){
		statement
	};
*/

// if Statement 

let numA =5;

if(numA < 3){
	console.log('Hello');
};

let city = "New York";

if(city === "New York") {
	console.log("Welcome to New York City!");
}

// else if Clause

/*
	- executes a statement if previous condition are false and if the specific condition is true
	- "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.
*/

//else if() statement was no longer ran because the if() state was able to run and the evaluation of the whole state stios there 

let numB = 1;

if (numA > 3){
	console.log('Hello');
} else if (numB > 0){
	console.log('World');
}

//=======================================

if (numA < 3){
	console.log('Hello');
} else if (numB > 0){
	console.log('World');
}


// else if in string

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York");
} else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan");
}


// else statement

/*
	- Executes a statement if all other conditions are false
	- The "else" statement is optional and can be added to capture any other result to change the flow of a program.
*/

let numC = -5;
let numD = 7;

if (numC > 0){
	console.log ('Hello');
} else if ( numD === 0) {
	console.log ('World');
} else {
	console.log ('Again');
}

// if, else if, and else Statement with function

function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return 'Not a typhoon yet.';
	} else if (windSpeed <= 61) {
		return	'Tropical depression detected.';
	} else if (windSpeed >= 62 && windSpeed <=88) { // && means AND operator
		return 'Tropical storm detected';
	} else if (windSpeed >= 89 && windSpeed <= 117) { // || means OR operator
		return 'Severe Tropical Storm detected.';
	} else {
		return 'Typoon detected.';
	}
}

let message = determineTyphoonIntensity(69);
console.log(message);

if (message == 'Tropical storm detected'){
	console.warn(message);
}


// [SECTION] Conditional (Ternary Operator)

/*
	- The conditional (Ternary) operator takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy


	// Ternary operator is for shorthand code - Commonly used for single statement execution where the result consists of only one line of code.
	- Syntax:
	(expression) ? ifTrue : ifFalse;

	- can be used as an alternative to an "if else" statement.

*/

//Single statement execution

let t = "yes";
let f = "no";

let ternaryResult = (1 < 18) ? t : f;
console.log("Result of Ternary Operator: " + ternaryResult);

//Multiple statement execution

let name = prompt("What is your name?");

function isOfLegalAge() {
    return 'You are of the legal age limit';
}

function isUnderAge() {
    return 'You are under the age limit';
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// the "parseInt" function converts the input to Interger data type



// [SECTION] Switch Statement

/*
	The "switch" statements evaluates an expression and matches the expression's value to a case clause.
	The "switch" will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	- Can be used as an alternative to an "if", "else if" and "else" statement where the data to be used in the condition in an expected output.
*/
/*
	switch (expression) {
		case value:
			statement;
			break;
		default: statement;
	}
*/
let day = prompt("What day os the week is it today?").toLowerCase();
// .tolowerCase() function/method will change input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters.

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}